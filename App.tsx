import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { BottomTabNavigator } from "./src/navigation/TabNavigator";
// import { Button, View } from 'react-native';
//import DrawerNavigator from "./src/navigation/DrawerNavigator";
//import MainStackNavigator from "./src/navigation/StackNavigator";

export default function App() {
    return (
        <NavigationContainer>
            <BottomTabNavigator />
        </NavigationContainer>
    );
}