import * as React from 'react';
// import { Button, View } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from "../../src/screens/Login";
import Settings from "../../src/screens/Settings";
import Home from "../../src/screens/Home";
import Profile from "../../src/screens/Profile";
import Notifications from "../../src/screens/Notifications";

const Stack = createStackNavigator();

export default function MainStackNavigator () {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Login" component={Login}/>
            <Stack.Screen name="Home" component={Home}/>
            <Stack.Screen name="Profile" component={Profile}/>
        </Stack.Navigator>
    )
};

export function ProfileStackNavigator () {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Notifications" component={Notifications}/>
            <Stack.Screen name="Settings" component={Settings}/>
        </Stack.Navigator>
    )
};
