import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MainStackNavigator, { ProfileStackNavigator } from "./StackNavigator";
import { DrawerNavigator } from "../navigation/DrawerNavigator";

const Tab = createBottomTabNavigator();

export function BottomTabNavigator() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={MainStackNavigator} />
            <Tab.Screen name="Profile" component={ProfileStackNavigator} />
            <Tab.Screen name="Login" component={DrawerNavigator} />
        </Tab.Navigator>
    );
};