import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import MainStackNavigator from "./StackNavigator";
import Login from "../../src/screens/Login";
import Home from "../../src/screens/Home";

const Drawer = createDrawerNavigator();

export function DrawerNavigator() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Login" component={Login} />
            <Drawer.Screen name="Home" component={Home} />
        </Drawer.Navigator>
    );
}
