import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

// @ts-ignore
export default function Login({ navigation }) {
    return (
        <View style={styles.container}>
            <Text>Open up SSApp.tsx to start working on your app!</Text>
            <Button title="Login" onPress={() => navigation.navigate('Home')}>Login</Button>
            <Button title="Profile" onPress={() => navigation.navigate('Profile')}>Login</Button>
            <Button title="Notifications" onPress={() => navigation.navigate('Notifications')}>Login</Button>
            <Button title="Settings" onPress={() => navigation.navigate('Settings')}>Login</Button>
            <StatusBar style="auto" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
